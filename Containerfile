ARG BASE_IMAGE=
ARG BASE_TAG=
ARG HTTP_PROXY=
ARG HTTPS_PROXY=
ARG http_proxy=
ARG https_proxy=

FROM ${BASE_IMAGE}:${BASE_TAG}

LABEL maintainer="Stefan Schneider <eqsoft4@gmail.com>"

USER root

ENV DEBIAN_FRONTEND noninteractive
ENV TZ=Europe/Berlin
SHELL ["/bin/bash", "-c"]

ARG UID=65534
ARG GID=65534

COPY ./files/ /etc/ssh/

RUN <<EOF
set -e
apt-get update
apt-get install -y --no-install-recommends \
tzdata \
apt-transport-https \
ca-certificates \
software-properties-common \
nano \
gettext \
openssh-server
ln -snf /usr/share/zoneinfo/$TZ /etc/localtime
echo $TZ > /etc/timezone
passwd -d root
chown -R "${UID}:${GID}" /etc/ssh
chown -R "${UID}:${GID}" /run
chown "${UID}:${GID}" /usr/sbin/sshd
chmod 500 /usr/sbin/sshd
mkdir -p /config/ssh
touch /config/ssh/authorized_keys
chown -R "${UID}:${GID}" /config
chmod -R 0700 /config
chmod 0600 /config/ssh/authorized_keys
EOF

# a clean tunnel connection does not require a shell login
# but sshfs needs a shell for a nobody login connection
# p.e. if you want to run as nobody with shell:
# RUN  sed -i "s/nobody.*/nobody\:x\:65534\:65534\:nobody\:\/config\:\/bin\/ash/" /etc/passwd

USER ${UID}
STOPSIGNAL SIGQUIT
RUN ssh-keygen -A
CMD ["/usr/sbin/sshd","-D","-e"]